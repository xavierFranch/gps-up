> Nota preliminar: tots els comentaris de la plantilla són informatius i han de desaparéixer en la versió entregable

# SISTEMA ACME - PLA DE DESENVOLUPAMENT DE SOFTWARE #

> El propòsit del document de pla de desenvolupament de software és determinar l'esforç, cost i calendari a nivell de projecte (distingint només les 4 fases)


## 1. ORGANITZACIÓ I EQUIP ##

> Descriure breument l'organització i l'equip de treball (quins rols, quants treballadors de cada rol)

## 2. ESTIMACIÓ D'ESFORÇ ##

> Calcular el nombre d'hores del projecte. Useu un excel o altra eina per calcular UCPs i convertir a hores. Copieu aquí les taules (però entregueu també l'excel!)

## 3. ESTIMACIÓ DE COST ##

> Calculeu el cost de personal i la resta de coses. Useu un excel o altra eina per fer els càlculs. Copieu aquí les taules (però entregueu també l'excel!). 

## 4. PLA DE PROJECTE ##

> Cal organitzar aquesta secció amb la informació més rellevant de les fases. Per cada fase, dir: objectius, entregables més importants, quantes iteracions a cada fase, dates d'inici i finalització, esforç de cada rol a cada fase, etc.
