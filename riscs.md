> Nota preliminar: tots els comentaris de la plantilla són informatius i han de desaparéixer en la versió entregable

# SISTEMA ACME - RISCS #


> El propòsit del document de riscs és enumerar els riscs més importants, juntament amb les estratègies de mitigació i plans de contingència. Identifiqueu entre 6 i 8

> Els tipus més habituals són: riscs de negoci, riscs de projecte, riscs tecnològics


## RISC 001. Nom del risc (no més de 4-5 paraules) ##

### Descripció ###

> Descripció del risc

### Probabilitat ###

> Molt probable - probable - versemblant - poc probable - gens probable (no calen grans explicacions)
 
### Impacte ###

> Descripció de l'impacte del risc
 
### Indicadors ###

> Quins indicadors objectius permeten saber que el risc ha ocorregut o està a punt d'ocórrer (secció opcional, de vegades no està clar)
 
### Estratègies de mitigació ###

> Mesures per reduir la probabilitat d'ocurrença del risc
 
### Plans de mitigació ###

> Mesures per reduir l'impacte del risc quan es produeix

## RISC 002. Nom del risc (no més de 4-5 paraules) ##

etc...
